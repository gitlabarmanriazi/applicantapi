﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Project.DistributedService.WebHostCore
{
    public class Program
    {
        public static void Main(string[] args) => BuildWebHost(args).Run();
        public static bool DisableProfilingResults { get; internal set; }

        public static IWebHost BuildWebHost(string[] args) =>
            new WebHostBuilder()
                .UseKestrel(options => {
                    var ip = IPAddress.Parse("172.18.4.19");
                    options.Listen(ip, 8085);
                    options.AllowSynchronousIO = true;
                    options.Limits.MaxConcurrentConnections = 100;
                    options.Limits.MaxConcurrentUpgradedConnections = 100;
                    options.Limits.MaxRequestHeaderCount = 500;

                })
                .PreferHostingUrls(true)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;

                    var sharedFolder = Path.Combine(env.ContentRootPath, "Shared");

                    config
                        .AddJsonFile(Path.Combine(sharedFolder, "shared_settings.json"), optional: true) // When running using dotnet run
                        .AddJsonFile("appsettings.json", optional: true) // When app is published
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);// // When app is developing

                    config.AddEnvironmentVariables();
                })
                 .ConfigureLogging((hostingContext, logging) =>
                 {
                     logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                     logging.AddConsole(options => options.IncludeScopes = true);
                     logging.AddDebug();
                     logging.AddFilter((provider, category, logLevel) =>
                     {
                         if (provider == "Microsoft.Extensions.Logging.Console.ConsoleLoggerProvider" &&
                             category == "Project.DistributedService.WebHostCore.Controllers.BudgetProjectController")
                         {
                             return false;
                         }
                         if (provider == "Microsoft.Extensions.Logging.Console.ConsoleLoggerProvider" &&
                             category == "Project.DistributedService.WebHostCore.Controllers.PriceRepertoryController")
                         {
                             return false;
                         }
                         if (provider == "Microsoft.Extensions.Logging.Console.ConsoleLoggerProvider" &&
                            category == "Project.DistributedService.WebHostCore.Controllers.PluralComponentProjectManagementSystemProjectReportController")
                         {
                             return false;
                         }
                         return true;
                     });
                 })
                .UseDefaultServiceProvider((ctx, opts) => { /* elided for brevity */ })
                .UseStartup<Startup>()
                .Build();
    }
}
