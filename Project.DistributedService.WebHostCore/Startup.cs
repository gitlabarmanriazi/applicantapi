﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Project.Application.WebHostCore.Services.Internal;
using Framework.NikExtensions;
using StackExchange.Profiling.Storage;
using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.DotNet.PlatformAbstractions;
using Microsoft.Extensions.Logging;
using Core.ProjectInfrastructure.OData.Models;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.EntityFrameworkCore;
using Core.Domain.SeedWork.Model;
using Core.ProjectInfrastructure.OData.ORM;
using System.Collections.Generic;
using Core.ProjectInfrastructure.OData.Models.ORM;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OData.Edm;

using Microsoft.AspNet.OData.Formatter;
using System.Linq;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNet.OData.Batch;

namespace Project.DistributedService.WebHostCore
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }



        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            HostingEnvironment = env;

            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            if (env.IsDevelopment())
            {
                //builder.AddUserSecrets<Startup>();
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            //builder.AddEnvironmentVariables();
            Configuration = builder.Build();


        }

        //OData
        private static IEdmModel GetEdmModel(IApplicationBuilder app)
        {
            var builder = new ODataConventionModelBuilder(app.ApplicationServices);
            builder.EntitySet<Book>("Books");
            builder.EntitySet<Press>("Presses");

            builder.EntitySet<Order>("Orders");
            builder.EntitySet<OrderRow>("OrderRows");

            builder.EntitySet<TBL_User_Authenticate>("Security");

            var DoAuthenticateWithDapper = builder.EntityType<TBL_User_Authenticate>().Action("DoAuthenticateWithDapper");
            var DoAuthenticateWithEF = builder.EntityType<TBL_User_Authenticate>().Action("DoAuthenticateWithEF");
            var DoAuthenticateWithADO = builder.EntityType<TBL_User_Authenticate>().Action("DoAuthenticateWithADO");
            ////doAuthenticate.Parameter<String>("Role");
            //doAuthenticate.ReturnsCollectionFromEntitySet<IActionResult>("Security");

            return builder.GetEdmModel() as IEdmModel;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        //to configure the built-in container's services
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddExceptional(Configuration.GetSection("Exceptional"));
            //
            services.AddSwaggerGen(SwaggerHelper.ConfigureSwaggerGen);
            //
            services.AddExceptional(Configuration.GetSection("Exceptional:ErrorStore:ConnectionString"), settings =>
            {
                settings.UseExceptionalPageOnThrow = HostingEnvironment.IsDevelopment();
                settings.DefaultStore.Settings.ApplicationName = "ApplicantApi";

            });
            // Note .AddMiniProfiler() returns a IMiniProfilerBuilder for easy intellisense

            //services.AddMiniProfiler(options =>
            //{
            //    // All of this is optional. You can simply call .AddMiniProfiler() for all defaults

            //    // (Optional) Path to use for profiler URLs, default is /mini-profiler-resources
            //    options.RouteBasePath = "/profiler";

            //    // (Optional) Control storage
            //    // (default is 30 minutes in MemoryCacheStorage)
            //    (options.Storage as MemoryCacheStorage).CacheDuration = TimeSpan.FromMinutes(60);

            //    // (Optional) Control which SQL formatter to use, InlineFormatter is the default
            //    options.SqlFormatter = new StackExchange.Profiling.SqlFormatters.InlineFormatter();


            //});



            //var sp = services.BuildServiceProvider();
            //services.AddScoped<ISchema>(_ => new BankAppSchema(type => (GraphType)sp.GetService(type)) { Query = sp.GetService<BANKAppQuery>() });
            //
            //services.AddSingleton<ISchema>(s => new BankAppSchema(type => (GraphType)s.GetService(type)));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            #region NikServicesDIP
            //services.AddNikConnection();
            services.AddNikRepository();
            services.AddNikUnitOfWork();
            #endregion

            #region NikService
            // Register application services.
            services.AddTransient<IBudgetService, BudgetService>();
            services.AddTransient<IProjectManagementSystemService, ProjectManagementSystemService>();
            services.AddTransient<IPluralComponentProjectManagementSystemService, PluralComponentProjectManagementSystemService>();
            services.AddTransient<IPriceRepertoryService, PriceRepertoryService>();
            //                      
            #endregion

            services.AddScoped<IContext, DummyContext>();
            services.AddScoped<ISecurityContext, SecurityContext>();
            services.AddMvc();

            #region OData

            //services.AddDbContext<BookStoreContext>(opt => opt.UseInMemoryDatabase("BookLists"));            
            services.AddDbContext<SecurityEFContext>();

            services.AddOData();
            //services.AddMvcCore(options =>
            //{
            //    foreach (var outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
            //    {
            //        outputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
            //    }
            //    foreach (var inputFormatter in options.InputFormatters.OfType<ODataInputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
            //    {
            //        inputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
            //    }
            //}).AddApiExplorer();
            #endregion          

            //var sp = services.BuildServiceProvider();
            //services.AddTransient<ISchema>(_ => new Schema { Query = sp.GetService<BANKAppQuery>() });
            //var sp = services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            //app.UseDeveloperExceptionPage();
            //app.UseMvcWithDefaultRoute();

            if (env.IsDevelopment())
            {
                app.UseExceptional();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Exceptions");
            }


            // app.UseMvcWithDefaultRoute();
            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715
            //app.UseCors();



            app.UseODataBatching();
            app.UseMvc(
                routes =>
                {
                    routes.MapRoute(
                    name: "defaultWithArea",
                    template: "{area}/{controller=Home}/{action=Index}/{id?}");

                    routes.MapRoute(
                    name: "Http API V2",
                    template: "api/{controller=Home}/{action=Index}/{id?}");

                    routes.Select().Expand().Filter().OrderBy().MaxTop(100).Count();
                    routes.MapODataServiceRoute("odata", "odata", GetEdmModel(app), new DefaultODataBatchHandler());
                    routes.EnableDependencyInjection();
                });

            app.UseSwagger(SwaggerHelper.ConfigureSwagger);
            app.UseSwaggerUI(SwaggerHelper.ConfigureSwaggerUI);
            //app.UseMiniProfiler();
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }

        //private static RequestDelegate ChangeContextToHttps(RequestDelegate next)
        //{
        //    return async context =>
        //    {
        //        context.Request.Scheme = "https";
        //        await next(context);
        //    };
        //}
    }
}

