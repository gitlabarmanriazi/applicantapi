﻿using Microsoft.AspNetCore.Mvc;
using Project.Application.WebHostCore.Services.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.DistributedService.WebHostCore.Controllers
{
    [Route("api/[controller]/[action]")]
    public class PluralComponentProjectManagementSystemProjectReportController : Controller
    {
        private readonly IPluralComponentProjectManagementSystemService _pluralcomponentprojectManagementSystemprojectService;

        public PluralComponentProjectManagementSystemProjectReportController(IPluralComponentProjectManagementSystemService pluralcomponentprojectManagementSystemprojectService)
        {
            _pluralcomponentprojectManagementSystemprojectService = pluralcomponentprojectManagementSystemprojectService;
        }

        public class pluralcomponentprojectManagementSystemProjectReport
        {
            public string SfodFieldName { get; set; }
            public string SfoName { get; set; }
        }


        [HttpPost]
        [Produces("application/json", Type = typeof(pluralcomponentprojectManagementSystemProjectReport))]
        [RequestSizeLimit(100000000)]
        public async Task<IActionResult> GetInitializeStateProjectManagementSystemGrid([FromBody]pluralcomponentprojectManagementSystemProjectReport pluralcomponentprojectManagementSystemProjectReport)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var resultData = await _pluralcomponentprojectManagementSystemprojectService.GetInitializeStateProjectManagementSystemGrid(pluralcomponentprojectManagementSystemProjectReport.SfodFieldName, pluralcomponentprojectManagementSystemProjectReport.SfoName);

            if (resultData == null || resultData.Count() == 0)
            {
                return NotFound();
            }


            return Ok(resultData);
        }

        public class pluralcomponentUserProfileHistory
        {
            public string SubSystemId { get; set; }
            public string UphGridName { get; set; }
            public string UphDeleteDate { get; set; }
            public string UserId { get; set; }
        }

        [HttpPost]
        [Produces("application/json", Type = typeof(pluralcomponentUserProfileHistory))]
        [RequestSizeLimit(100000000)]
        public async Task<IActionResult> GetStateUserProfileHistoryGrid([FromBody]pluralcomponentUserProfileHistory pluralcomponentUserProfileHistory)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var resultData = await _pluralcomponentprojectManagementSystemprojectService.GetStateUserProfileHistoryGrid(pluralcomponentUserProfileHistory.SubSystemId, pluralcomponentUserProfileHistory.UphGridName, pluralcomponentUserProfileHistory.UphDeleteDate, pluralcomponentUserProfileHistory.UserId);

            if (resultData == null || resultData.Count() == 0)
            {
                return NotFound();
            }


            return Ok(resultData);
        }

        public class pluralcomponentAffectUserProfileHistory
        {
            public  int? SsID_fk { get; set; }
            public long? SfoID_fk { get; set; }          
            public  string UphGridName { get; set; }
            public  byte[] UphBody { get; set; }
            public  string UphNote { get; set; }
            public  string UphRegisterDate { get; set; }
            public  int UphType { get; set; }
            public  int? UphActive { get; set; }
            public  int? UphStatus { get; set; }
            public  string UphDeleteDate { get; set; }
            public  int? ACC_FinancialYearID { get; set; }
            public  int? UserID { get; set; }
            public int? UphID { get; set; }
        }

        [HttpPost]
        [Produces("application/json", Type = typeof(pluralcomponentAffectUserProfileHistory))]
        [RequestSizeLimit(100000000)]
        public async Task<IActionResult> PostStateUserProfileHistoryGrid([FromBody]pluralcomponentAffectUserProfileHistory pluralcomponentAffectUserProfileHistory)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var resultData = await _pluralcomponentprojectManagementSystemprojectService.PostStateUserProfileHistoryGrid(
                pluralcomponentAffectUserProfileHistory.SsID_fk,
                pluralcomponentAffectUserProfileHistory.SfoID_fk,
                pluralcomponentAffectUserProfileHistory.UphGridName,
                pluralcomponentAffectUserProfileHistory.UphBody,
                pluralcomponentAffectUserProfileHistory.UphNote,
                pluralcomponentAffectUserProfileHistory.UphRegisterDate,
                pluralcomponentAffectUserProfileHistory.UphType,
                pluralcomponentAffectUserProfileHistory.UphActive,
                pluralcomponentAffectUserProfileHistory.UphStatus,
                pluralcomponentAffectUserProfileHistory.UphDeleteDate,
                pluralcomponentAffectUserProfileHistory.ACC_FinancialYearID,
                pluralcomponentAffectUserProfileHistory.UserID,
                pluralcomponentAffectUserProfileHistory.UphID);

            if (resultData == null || resultData.Count() == 0)
            {
                return NotFound();
            }


            return Ok(resultData);
        }
    }
}
