﻿using Core.Domain.SeedWork.Model;
using Core.ProjectInfrastructure.OData.ORM;
using Microsoft.AspNet.OData;
using Project.Application.WebHostCore.Services.Internal;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData.Routing;
using System.Threading.Tasks;

namespace Project.DistributedService.WebHostCore.Controllers
{
    //[ODataRoute("[controller]/[action]")]
    public class SecurityController : ODataController
    {

        private ISecurityContext _context;
        private SecurityEFContext _efcontext;        
        private readonly IBudgetService _budprojectService;
        public SecurityController(ISecurityContext context, SecurityEFContext efcontext, IBudgetService budprojectService)
        {
            _budprojectService = budprojectService;
            _efcontext = efcontext;
        }

        [EnableQuery]
        public IQueryable Get() => _context.Securities;

        [EnableQuery]
        public IActionResult DoAuthenticateWithDapper()
        {
            string userName = "r_monavari";
            string userPassword = "123";
            string acc_FinancialYearsID = "1397";
            string tokenId = "";
            TBL_User_Authenticate queryResult = null;
            //Dictionary<string, object> resultSet;
            
            var dsResultSet = _budprojectService.AuthenticateDappper(userName, userPassword, acc_FinancialYearsID, ref tokenId, ref queryResult);
            // خواندن توکن
            //tokenId = query.queryResult.Values.ToArray()[0].ToString();

            return Ok(dsResultSet);
        }
        [EnableQuery]
        public IActionResult DoAuthenticateWithADO()
        {
            string userName = "r_monavari";
            string userPassword = "123";
            string acc_FinancialYearsID = "1397";
            string tokenId = "";
            TBL_User_Authenticate queryResult = null;
            //Dictionary<string, object> resultSet;

            var dsResultSet = _budprojectService.AuthenticateADO(userName, userPassword, acc_FinancialYearsID, ref tokenId, ref queryResult);
            // خواندن توکن
            //tokenId = query.queryResult.Values.ToArray()[0].ToString();

            return Ok(dsResultSet);
        }
        [EnableQuery]
        public async Task<IActionResult> DoAuthenticateWithEF()
        {
            string userName = "r_monavari";
            string userPassword = "123";
            string acc_FinancialYearsID = "1397";
            string tokenId = "";
            TBL_User_Authenticate queryResult = null;

          


            var dsResultSet = _budprojectService.AuthenticateEF(userName, userPassword, acc_FinancialYearsID, ref tokenId, ref queryResult);
            // خواندن توکن
            //tokenId = query.queryResult.Values.ToArray()[0].ToString();

            return Ok(dsResultSet);
        }
    }
}
    //[Route("api/[controller]/[action]")]
    //public class AuthenticationController : Controller
    //{

    //    private readonly IBudgetService _budprojectService;

    //    public AuthenticationController(IBudgetService budprojectService)
    //    {
    //        _budprojectService = budprojectService;
    //    }

    //    public class Authenticate
    //    {
    //        public string userName { get; set; }
    //        public string userPassword { get; set; }
    //        public string acc_FinancialYearsID { get; set; }
    //        public  string tokenId { get; set; }
    //        public  ActionResult queryResult { get; set; }
    //    }
    //    public class AuthoInfo
    //    {            
    //        public string ClientBrowser { get; set; }
    //        public string ClientIP { get; set; }
    //        public string ClientMacAddress { get; set; }
    //        public string DeviceName { get; set; }            
    //        public string ClientComputerName { get; set; }


    //    }
    //    [HttpPost]
    //    [Produces("application/json")]
    //    public IEnumerable<dynamic> DoAuthenticate()
    //    {
    //        string userName = "fan";
    //        string userPassword = "123";
    //        string acc_FinancialYearsID = "1395";
    //        string  tokenId="";
    //        ActionResult queryResult=null;

    //        var dsResultSet = _budprojectService.Authenticate(userName, userPassword, acc_FinancialYearsID, ref tokenId, ref queryResult);

    //            // خواندن توکن
    //        //tokenId = query.queryResult.Values.ToArray()[0].ToString();

    //        return dsResultSet;
    //    }
    //}

