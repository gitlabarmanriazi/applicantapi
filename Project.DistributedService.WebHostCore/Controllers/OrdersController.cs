﻿using System.Linq;
using Core.ProjectInfrastructure.OData.Models;
using Core.ProjectInfrastructure.OData.ORM;
using Microsoft.AspNet.OData;

namespace Project.DistributedService.WebHostCore.Controllers
{
    public class OrdersController : ODataController
    {
        private IContext _context;
        public OrdersController(IContext context) => _context = context;

        [EnableQuery]
        public IQueryable Get() => _context.Orders;

        [EnableQuery]
        public SingleResult Get(int key) => SingleResult.Create(_context.Orders.Cast<Order>().Where(o => o.Id.Equals(key)));

        [EnableQuery]
        public IQueryable GetOrderRows(int key) => _context.OrderRows.Cast<OrderRow>().Where(r => r.OrderId.Equals(key));
    }
}