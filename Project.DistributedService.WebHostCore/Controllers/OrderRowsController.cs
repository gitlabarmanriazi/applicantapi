﻿using Core.ProjectInfrastructure.OData.Models;
using Core.ProjectInfrastructure.OData.ORM;
using Microsoft.AspNet.OData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Project.DistributedService.WebHostCore.Controllers
{
    namespace ODataDemo.Controllers
    {
        public class OrderRowsController : ODataController
        {
            private IContext _context;
            public OrderRowsController(IContext context) => _context = context;

            [EnableQuery]
            public IQueryable Get() => _context.OrderRows;

            [EnableQuery]
            public SingleResult Get(int key) => SingleResult.Create(_context.OrderRows.Cast<OrderRow>().Where(o => o.Id.Equals(key)));

            [EnableQuery]
            public SingleResult Get(int key, int relatedKey) => SingleResult.Create(_context.OrderRows.Cast<OrderRow>().Where(r => r.Id.Equals(key) && r.OrderId.Equals(relatedKey)));
        }
    }
}
