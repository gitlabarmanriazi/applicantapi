﻿using Core.ProjectInfrastructure.OData.Models;
using Core.ProjectInfrastructure.OData.Models.ORM;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using Project.Application.WebHostCore.Services.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.DistributedService.WebHostCore.Controllers
{
    //[Produces("application/json")]
    //[Route("api/books")]
    public class BooksController : ODataController
    {
        private BookStoreContext _db;
        private readonly IBudgetService _budprojectService;
        public BooksController(BookStoreContext context, IBudgetService budprojectService)
        {
            _budprojectService = budprojectService;
            _db = context;
   
            if (context.Books.Count() == 0)
            {
                foreach (var b in DataSource.GetBooks())
                {
                    context.Books.Add(b);
                    context.Press.Add(b.Press);
                }
                context.SaveChanges();
            }
        }
      
        [EnableQuery]
        public IActionResult Get()
        {
            return Ok(_db.Books);
        }

        [EnableQuery]
        public IActionResult Get(int key)
        {
            return Ok(_db.Books.FirstOrDefault(c => c.Id == key));
        }
      
        [EnableQuery]
        public IActionResult Post([FromBody]Book book)
        {
            _db.Books.Add(book);
            _db.SaveChanges();
            return Created(book);
        }
        //[EnableQuery]
        //public IActionResult Post()
        //{
        //    string userName = "r_monavari";
        //    string userPassword = "123";
        //    string acc_FinancialYearsID = "1397";
        //    string tokenId = "";
        //    TBL_User_Authenticate queryResult = null;
        //    //Dictionary<string, object> resultSet;

        //    var dsResultSet = _budprojectService.Authenticate(userName, userPassword, acc_FinancialYearsID, ref tokenId, ref queryResult);
        //    // خواندن توکن
        //    //tokenId = query.queryResult.Values.ToArray()[0].ToString();

        //    return Ok(dsResultSet);
        //}

    }
}
