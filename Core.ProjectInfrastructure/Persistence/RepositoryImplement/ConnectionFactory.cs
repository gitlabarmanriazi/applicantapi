﻿using Core.Domain.SeedWork.Repository;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System;
using Microsoft.Extensions.Configuration;

namespace Core.ProjectInfrastructure.Persistence
{
  
    public sealed class ConnectionIFactory //: IDisposable
    {             
        public SqlConnection _sqlcon;
        private static ConnectionIFactory instance = null;
        private static readonly object padlock = new object();

        ConnectionIFactory(SqlConnection sqlcon)
        {
            _sqlcon = sqlcon;
        }

     
        public static ConnectionIFactory Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {

                        instance = new ConnectionIFactory(new SqlConnection("Data Source = 172.18.4.75; Initial Catalog = MaliAlborz; User ID = sa; Password = zse098$;"));
                        
                    }
                    return instance;
                }
            }
        }
        

       
    }


    public sealed class ConnectionIFactoryDocument //: IDisposable
    {
        public SqlConnection _sqlcon;
        private static ConnectionIFactoryDocument instance = null;
        private static readonly object padlock = new object();

        ConnectionIFactoryDocument(SqlConnection sqlcon)
        {
            _sqlcon = sqlcon;
        }
        public static ConnectionIFactoryDocument InstanceDocument
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {

                        instance = new ConnectionIFactoryDocument(new SqlConnection("Data Source = 172.18.4.75; Initial Catalog = DocumentDatabase; User ID = sa; Password = zse098$;"));
                    }
                    return instance;
                }
            }
        }
    }    
    }
