﻿using Core.Domain.SeedWork.Model;
using Core.Domain.SeedWork.Repository;
using Core.Domain.SeedWork.Repository.IEntitysRepository;
using Core.ProjectInfrastructure.OData.Models;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Core.ProjectInfrastructure.Persistence.RepositoryImplement.EntitiesRepository
{
  
 
    public class BudgetProjectRepository : Repository,IBudgetProjectRepository
    {
        

        public BudgetProjectRepository()
        {            
        }


        public async Task<IEnumerable<dynamic>> FindByDapperQuery(string query)
        {
            var storeProcedureName = "[dbo].[BUD_Project_DdlSelect]";            
            var param = new DynamicParameters();
            param.Add("WhereClause1", query);
            param.Add("WhereClause2", null);
            param.Add("WhereClause3", null);
            param.Add("WhereClause4", null);
            param.Add("SortExperssion", null);
            param.Add("TBL_UserID", 1);
            param.Add("ACC_FinancialYearID", null);
            param.Add("TypeOperation", 7);
            param.Add("SQLOut", null);
            var list =await SqlMapper.QueryAsync(ConnectionIFactory.Instance._sqlcon, storeProcedureName, param, commandType: CommandType.StoredProcedure);
            return list;

            //p.Add("@b", dbType: DbType.Int32, direction: ParameterDirection.Output);
            //int b = p.Get<int>("@b");
        }

        public async Task<IEnumerable<dynamic>> FindByDapperQueryIdentityProjectPlan(string query)
        {
            var storeProcedureName = "[dbo].[BUD_Project_SelectByPK]";
            var param = new DynamicParameters();
            param.Add("BUD_ProjectID", query);           
            var list = await SqlMapper.QueryAsync(ConnectionIFactory.Instance._sqlcon, storeProcedureName, param, commandType: CommandType.StoredProcedure);
            return list;
        }



        Task<IEnumerable<dynamic>> IReadOnlyRepository.FindAll()
        {
            throw new System.NotImplementedException();
        }

        Task<IEnumerable<dynamic>> IReadOnlyRepository.FindByConditionalQuery(string id, string where, string sort)
        {
            throw new System.NotImplementedException();
        }

        public    Domain.SeedWork.Model.TBL_User_Authenticate FindByDapperAuthenticateQuery(string userName, string userPassword, string acc_FinancialYearsID, ref string tokenId, ref Domain.SeedWork.Model.TBL_User_Authenticate queryResult)
        {
            var storeProcedureName = "[dbo].[TBL_User_Authenticate]";
            var param = new DynamicParameters();
            var user = new Domain.SeedWork.Model.TBL_User_Authenticate();
            param.Add("UserName", userName);
            param.Add("UserPassword", userPassword);
            param.Add("Epoch", 0);
            param.Add("IsAuthenticateWithActiveDirectory", 0);
            param.Add("TypeOperation", 5);
            param.Add("Acc_FinancialYearsID", acc_FinancialYearsID);
            param.Add("UserToken", tokenId, dbType: DbType.String, direction: ParameterDirection.Output);

            var reader = SqlMapper.QueryMultiple(ConnectionIFactory.Instance._sqlcon, storeProcedureName, param, commandType: CommandType.StoredProcedure);
            {
                user = reader.Read<Domain.SeedWork.Model.TBL_User_Authenticate>().AsList()[0];
            }
            user.UserToken = param.Get<string>("@UserToken");
            return user;
        }
        public Domain.SeedWork.Model.TBL_User_Authenticate FindByADOAuthenticateQuery(string userName, string userPassword, string acc_FinancialYearsID, ref string tokenId, ref Domain.SeedWork.Model.TBL_User_Authenticate queryResult)
        {

            var storeProcedureName = "[dbo].[TBL_User_Authenticate]";            
            var user = new Domain.SeedWork.Model.TBL_User_Authenticate();
            SqlCommand cmd = new SqlCommand(storeProcedureName, ConnectionIFactory.Instance._sqlcon);
            cmd.CommandType = CommandType.StoredProcedure;
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("UserName", SqlDbType.NVarChar) { Value = userName });            
            parameters.Add(new SqlParameter("UserPassword", SqlDbType.NVarChar) { Value = userPassword });
            parameters.Add(new SqlParameter("Epoch", SqlDbType.NVarChar) { Value = 0 });
            parameters.Add(new SqlParameter("IsAuthenticateWithActiveDirectory", SqlDbType.NVarChar) { Value = 0 });
            parameters.Add(new SqlParameter("TypeOperation", SqlDbType.NVarChar) { Value = 5 });
            parameters.Add(new SqlParameter("Acc_FinancialYearsID", SqlDbType.NVarChar) { Value = acc_FinancialYearsID });            

            SqlParameter paramUserToken = new SqlParameter("UserToken", SqlDbType.NVarChar);
            paramUserToken.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(paramUserToken);

            DataTable dt=null;
            //using (SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd))
            //{
            //    sqlAdapter.Fill(dt);
            //}
            DataTable dtRes=null;
            if (parameters != null)
            {
                cmd.Parameters.AddRange(parameters.ToArray());
            }

            try
            {
                ConnectionIFactory.Instance._sqlcon.Open();
            }
            catch
            {

            }

            var sqlDataReader = cmd.ExecuteNonQuery();
          

            return user;
        }
        Task<dynamic> IReadOnlyRepository.FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        Task<IEnumerable<dynamic>> IReadOnlyRepository.FindByPredicate(System.Linq.Expressions.Expression<System.Func<dynamic, bool>> predicate)
        {
            throw new System.NotImplementedException();
        }

        Task<IEnumerable<dynamic>> IReadOnlyRepository.FindByQuery(string query)
        {
            throw new System.NotImplementedException();
        }
    }
}
