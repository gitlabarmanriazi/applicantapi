﻿using Core.Domain.SeedWork.Repository;
using Core.Domain.SeedWork.Repository.IEntitysRepository;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Core.ProjectInfrastructure.Persistence.RepositoryImplement.EntitiesRepository
{
    public class PluralComponentProjectManagementSystemRepository : Repository, IPluralComponentProjectManagementSystemRepository
    {

        public PluralComponentProjectManagementSystemRepository()
        {
        }

        public async Task<IEnumerable<dynamic>> FindByDapperQuery(string query,string name)
        {
            var storeProcedureName = "[dbo].[NIK_Sfod_SelectAll]";
            var param = new DynamicParameters();
            
            param.Add("Fields", "[dbo].[NIK_SystemFormObjectsDetail].*,[dbo].[NIK_SystemFormObjects].NIK_SfoCustomStyle");
            param.Add("JoinClause", " inner join [dbo].[NIK_SystemFormObjects] on [dbo].[NIK_SystemFormObjectsDetail].NIK_SfoID_fk = NIK_SystemFormObjects.NIK_SfoID ");
            var tempString=query.Split(",");
            string outputString = ""; 
            foreach (var item in tempString)
            {
                outputString += "'"+item+"',";
            }
            outputString=outputString.Remove(outputString.Length-1,1);
            param.Add("WhereClause", "NIK_SfodFieldName IN ("+ outputString + ")  and  NIK_SystemFormObjects.NIK_SfoName='"+ name.Trim()+ "' and [NIK_SystemFormObjects].NIK_SfoActive= 1 and [NIK_SystemFormObjectsDetail].NIK_SfodActive=1");
            param.Add("SortExpression", null);

            var list = await SqlMapper.QueryAsync(ConnectionIFactory.Instance._sqlcon, storeProcedureName, param, commandType: CommandType.StoredProcedure);
            return list;
        }
        public async Task<IEnumerable<dynamic>> FindByDapperQueryUserProfileHistory(string subSystemId,string gridName, string codeMeli, string userId)
        {
            var storeProcedureName = "[dbo].[TBL_Uph_SelectAll]";
            var param = new DynamicParameters();
            string outputString = " TBL_SsID_fk="+ subSystemId + " and TBl_UserID=" + userId + "";

            param.Add("Fields", "*");            
            param.Add("JoinClause",null);

            if (!string.IsNullOrEmpty(gridName)) 
            outputString = outputString + " and TBL_UphGridName ='"+ gridName+"'";

            if (!string.IsNullOrEmpty(codeMeli))
                outputString = outputString + " and TBL_UphDeleteDate ='" + codeMeli + "'";

            param.Add("WhereClause", outputString);
            param.Add("SortExpression",null);
          
            var list = await SqlMapper.QueryAsync(ConnectionIFactory.Instance._sqlcon, storeProcedureName, param, commandType: CommandType.StoredProcedure);
            return list;
        }
        public async Task<IEnumerable<dynamic>> AffectByDapperQueryUserProfileHistory(
                                    int? ssID_fk,
                                    long? sfoID_fk,
                                    string uphGridName,
                                    byte[] uphBody,
                                    string uphNote,
                                    string uphRegisterDate,
                                    int? uphType,
                                    int? uphActive,
                                    int? uphStatus,
                                    string uphDeleteDate,
                                    int? financialYearID,
                                    int? userID,
                                    int? uphID)
        {
            var storeProcedureName = "[dbo].[TBL_Uph_InsertSpecific]";
            var param = new DynamicParameters();
           param.Add("TBL_SsID_fk", ssID_fk);
           param.Add("NIK_SfoID_fk", sfoID_fk);
           param.Add("TBL_UphGridName", uphGridName);
           param.Add("TBL_UphBody ", uphBody);
           param.Add("TBL_UphNote ", uphNote);
           param.Add("TBL_UphRegisterDate ", uphRegisterDate);
           param.Add("TBL_UphType ", uphType);
           param.Add("TBL_UphActive ", uphActive);
           param.Add("TBL_UphStatus ", uphStatus);
           param.Add("TBL_UphDeleteDate ", uphDeleteDate);
           param.Add("ACC_FinancialYearID ",financialYearID);
           param.Add("TBL_UserID ", userID);
           param.Add("TBL_UphID ", uphID);

            var list = await SqlMapper.QueryAsync(ConnectionIFactory.Instance._sqlcon, storeProcedureName, param, commandType: CommandType.StoredProcedure);
            return list;
        }
        Task<IEnumerable<dynamic>> IReadOnlyRepository.FindAll()
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<dynamic>> IReadOnlyRepository.FindByConditionalQuery(string id, string where, string sort)
        {
            throw new NotImplementedException();
        }

        Task<dynamic> IReadOnlyRepository.FindById(int id)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<dynamic>> IReadOnlyRepository.FindByPredicate(System.Linq.Expressions.Expression<Func<dynamic, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<dynamic>> IReadOnlyRepository.FindByQuery(string query)
        {
            throw new NotImplementedException();
        }
    }
}
