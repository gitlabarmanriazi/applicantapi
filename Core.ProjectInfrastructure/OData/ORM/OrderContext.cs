﻿using Core.ProjectInfrastructure.OData.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.ProjectInfrastructure.OData.ORM
{
    public interface IContext
    {
        IQueryable Orders { get; set; }
        IQueryable OrderRows { get; set; }
    }

    public class DummyContext : IContext
    {
        public IQueryable Orders { get; set; }
        public IQueryable OrderRows { get; set; }

        public DummyContext()
        {
            var orders = new List<Order>()
            {
                new Order
                {
                    Id = 1,
                    Timestamp = DateTime.Now.AddDays(-2),
                    OrderRow = new OrderRow{
                                Id = 1,
                                OrderId = 1,
                                Qty = 1,
                                Description = "Book",
                                Price = 19.99M
                     }
                },
                new Order
                {
                    Id = 2,
                    Timestamp = DateTime.Now.AddDays(-1),
                    OrderRow = new OrderRow{
                                    Id = 2,
                                    OrderId = 1,
                                    Qty = 2,
                                    Description = "Pen",
                                    Price = 1.50M
                                }                    
                }
            };
            Orders = orders.AsQueryable();

            var orderRows = new List<OrderRow>()
            {
                new OrderRow
                {
                    Id = 1,
                    OrderId = 1,
                    Qty = 1,
                    Description = "Book",
                    Price = 19.99M
                },
                new OrderRow
                {
                    Id = 2,
                    OrderId = 1,
                    Qty = 2,
                    Description = "Pen",
                    Price = 1.50M
                },
                new OrderRow
                {
                    Id = 3,
                    OrderId = 2,
                    Qty = 1,
                    Description = "Ruler",
                    Price = 2M
                }
            };
            OrderRows = orderRows.AsQueryable();
        }
    }
}
