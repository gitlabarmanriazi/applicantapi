﻿using Core.Domain.SeedWork.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.ProjectInfrastructure.OData.ORM
{
    public interface ISecurityContext
    {
        IQueryable Securities { get; set; }        
    }

    public class SecurityContext : ISecurityContext
    {
        public IQueryable Securities { get; set; }

        public SecurityContext()
        {
            var rows = new List<TBL_User_Authenticate>()
            {
                new TBL_User_Authenticate
                {
                   Id=1,
                   UserName="arman",
                },
                new TBL_User_Authenticate
                {
                    Id=1,
                    UserName="arman2",
                }
            };
            Securities = rows.AsQueryable();
        }
    }


      
}
