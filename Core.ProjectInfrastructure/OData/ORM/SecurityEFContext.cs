﻿using Core.Domain.SeedWork.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;

namespace Core.ProjectInfrastructure.OData.ORM
{
 
    public class SecurityEFContext : DbContext
    {
        public SecurityEFContext(DbContextOptions<SecurityEFContext> options)
          : base(options)
        {
        }

        public DbSet<TBL_User_Authenticate> Securities { get; set; }       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // modelBuilder.Entity<TBL_User_Authenticate>().ToTable("TBL_User_Authenticate");

            modelBuilder.Entity<TBL_User_Authenticate>(e =>
            e.HasKey(g => g.PersonnelId)
            );
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Blogging;Trusted_Connection=True;");
        //    }
        //}


    }
}
