﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;



namespace Core.ProjectInfrastructure.OData.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public virtual OrderRow OrderRow { get; set; }
        public virtual List<OrderRow> OrderRows { get; set; }
    }
}
