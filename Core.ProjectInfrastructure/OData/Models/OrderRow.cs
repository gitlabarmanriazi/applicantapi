﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.ProjectInfrastructure.OData.Models
{
    public class OrderRow
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Order")]
        public int OrderId { get; set; }
        public int Qty { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}