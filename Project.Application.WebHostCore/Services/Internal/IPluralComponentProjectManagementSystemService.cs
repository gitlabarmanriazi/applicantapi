﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.WebHostCore.Services.Internal
{    
  public interface IPluralComponentProjectManagementSystemService
    {
        Task<IEnumerable<dynamic>> GetInitializeStateProjectManagementSystemGrid(string sfodFieldName,string sfoName);
        Task<IEnumerable<dynamic>> GetStateUserProfileHistoryGrid(string subSystemId, string gridName, string codeMeli, string userId);
        Task<IEnumerable<dynamic>> PostStateUserProfileHistoryGrid(
                                    int? ssID_fk,
                                    long? sfoID_fk,
                                    string uphGridName,
                                    byte[] uphBody,
                                    string uphNote,
                                    string uphRegisterDate,
                                    int? uphType,
                                    int? uphActive,
                                    int? uphStatus,
                                    string uphDeleteDate,
                                    int? financialYearID,
                                    int? userID,
                                    int? uphID);
    }

}
