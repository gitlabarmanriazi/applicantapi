﻿using Core.Domain.SeedWork.Model;
using Core.Domain.SeedWork.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Application.WebHostCore.Services.Internal
{
    public class BudgetService : IBudgetService
    {
        private readonly IUnitOfWorkForBudget _unitOfWork;
        public BudgetService(IUnitOfWorkForBudget unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public TBL_User_Authenticate AuthenticateDappper(string userName, string userPassword, string acc_FinancialYearsID, ref string tokenId, ref TBL_User_Authenticate queryResult)
        {
            var result = _unitOfWork.BudgetProjectRepository.FindByDapperAuthenticateQuery(userName, userPassword, acc_FinancialYearsID, ref tokenId, ref queryResult);
            return result;
        }
        public TBL_User_Authenticate AuthenticateADO(string userName, string userPassword, string acc_FinancialYearsID, ref string tokenId, ref TBL_User_Authenticate queryResult)
        {
            var result = _unitOfWork.BudgetProjectRepository.FindByADOAuthenticateQuery(userName, userPassword, acc_FinancialYearsID, ref tokenId, ref queryResult);
            return result;
        }
        public TBL_User_Authenticate AuthenticateEF(string userName, string userPassword, string acc_FinancialYearsID, ref string tokenId, ref TBL_User_Authenticate queryResult)
        {
            var result = _unitOfWork.BudgetProjectRepository.FindByADOAuthenticateQuery(userName, userPassword, acc_FinancialYearsID, ref tokenId, ref queryResult);
            return result;
        }
        public async Task<IEnumerable<dynamic>> Login(string codeMeli)
        {
            var result = await _unitOfWork.BudgetProjectRepository.FindByDapperQuery(codeMeli);
            return result;
        }
        public async Task<IEnumerable<dynamic>> GetBudgetProjectPlanTextFields(string query)
        {
            var result = await _unitOfWork.BudgetProjectRepository.FindByDapperQueryIdentityProjectPlan(query);
            return result;
        }

    }
}
