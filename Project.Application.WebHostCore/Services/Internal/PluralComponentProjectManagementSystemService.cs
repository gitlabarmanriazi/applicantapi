﻿using Core.Domain.SeedWork.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.WebHostCore.Services.Internal
{
    public class PluralComponentProjectManagementSystemService : IPluralComponentProjectManagementSystemService
    {
        private readonly IUnitOfWorkForProjectManagementSystem _unitOfWork;
        public PluralComponentProjectManagementSystemService(IUnitOfWorkForProjectManagementSystem unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<dynamic>> GetInitializeStateProjectManagementSystemGrid(string sfodFieldName, string sfoName)
        {
            var result = await _unitOfWork.PluralComponentProjectManagementSystemRepository.FindByDapperQuery(sfodFieldName, sfoName);
            return result;
        }
        public async Task<IEnumerable<dynamic>> GetStateUserProfileHistoryGrid(string subSystemId, string gridName, string codeMeli, string userId)
        {
            var result = await _unitOfWork.PluralComponentProjectManagementSystemRepository.FindByDapperQueryUserProfileHistory(subSystemId,gridName, codeMeli, userId);
            return result;
        }
        public async Task<IEnumerable<dynamic>> PostStateUserProfileHistoryGrid(
                                    int? ssID_fk,
                                    long? sfoID_fk,
                                    string uphGridName,
                                    byte[] uphBody,
                                    string uphNote,
                                    string uphRegisterDate,
                                    int? uphType,
                                    int? uphActive,
                                    int? uphStatus,
                                    string uphDeleteDate,
                                    int? financialYearID,
                                    int? userID,
                                    int? uphID)
        {
            var result = await _unitOfWork.PluralComponentProjectManagementSystemRepository.AffectByDapperQueryUserProfileHistory(ssID_fk,sfoID_fk, uphGridName, uphBody, uphNote, uphRegisterDate,uphType,uphActive, uphStatus, uphDeleteDate, financialYearID, userID,uphID);
            return result;
        }
    }
}
