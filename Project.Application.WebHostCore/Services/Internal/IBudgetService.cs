﻿using Core.Domain.SeedWork.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Application.WebHostCore.Services.Internal
{
    public interface IBudgetService
    {
        TBL_User_Authenticate AuthenticateDappper(string userName, string userPassword, string acc_FinancialYearsID, ref string tokenId, ref TBL_User_Authenticate queryResult);
        TBL_User_Authenticate AuthenticateEF(string userName, string userPassword, string acc_FinancialYearsID, ref string tokenId, ref TBL_User_Authenticate queryResult);
        TBL_User_Authenticate AuthenticateADO(string userName, string userPassword, string acc_FinancialYearsID, ref string tokenId, ref TBL_User_Authenticate queryResult);

        Task<IEnumerable<dynamic>> Login(string codeMeli);
        Task<IEnumerable<dynamic>> GetBudgetProjectPlanTextFields(string query);

    }
}
