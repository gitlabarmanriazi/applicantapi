﻿using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.NikExtensions
{
    public static class NikODataApplicationBuilder
    {

        public static IApplicationBuilder UseODataMvc(this IApplicationBuilder app, Action<IRouteBuilder> setupAction)
        {
            return app
                .UseODataBatching()
                .UseRouter(builder =>
                {
                    builder.DefaultHandler = app.ApplicationServices.GetRequiredService<MvcRouteHandler>();
                    setupAction(builder);
                });
        }
    }
}
