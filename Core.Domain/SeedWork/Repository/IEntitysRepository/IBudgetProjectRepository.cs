﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.Domain.SeedWork.Model;
using Dapper;
using Microsoft.AspNetCore.Mvc;

namespace Core.Domain.SeedWork.Repository.IEntitysRepository
{

    public interface IBudgetProjectRepository : IRepository
    {
        TBL_User_Authenticate FindByDapperAuthenticateQuery(string userName, string userPassword, string acc_FinancialYearsID, ref string tokenId, ref TBL_User_Authenticate queryResult);
        TBL_User_Authenticate FindByADOAuthenticateQuery(string userName, string userPassword, string acc_FinancialYearsID, ref string tokenId, ref TBL_User_Authenticate queryResult);

        Task<IEnumerable<dynamic>> FindByDapperQuery(string query);
        Task<IEnumerable<dynamic>> FindByDapperQueryIdentityProjectPlan(string query);
    }
}