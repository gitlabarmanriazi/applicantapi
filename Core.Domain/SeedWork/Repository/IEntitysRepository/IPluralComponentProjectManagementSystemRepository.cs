﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.SeedWork.Repository.IEntitysRepository
{

    public interface IPluralComponentProjectManagementSystemRepository : IRepository
    {
        Task<IEnumerable<dynamic>> FindByDapperQuery(string query,string name);
        Task<IEnumerable<dynamic>> FindByDapperQueryUserProfileHistory(string subSystemId,string gridName, string codeMeli, string userId);
        Task<IEnumerable<dynamic>> AffectByDapperQueryUserProfileHistory(
                                    int? ssID_fk,
                                    long? sfoID_fk,
                                    string uphGridName,
                                    byte[] uphBody,
                                    string uphNote,
                                    string uphRegisterDate,
                                    int? uphType,
                                    int? uphActive,
                                    int? uphStatus,
                                    string uphDeleteDate,
                                    int? financialYearID,
                                    int? userID,
                                    int? uphID);



    }

}
