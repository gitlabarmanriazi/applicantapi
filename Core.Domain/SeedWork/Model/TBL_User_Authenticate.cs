﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core.Domain.SeedWork.Model
{
    [Table("TBL_User_Authenticate")]
    public class TBL_User_Authenticate
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string CustomerTitle { get; set; }
        public bool IsActive { get; set; }
        public string LastLoginDate { get; set; }
        public string PersonnelId { get; set; }
        public string UserToken { get; set; }

    }
}
